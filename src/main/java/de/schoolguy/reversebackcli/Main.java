package de.schoolguy.reversebackcli;

import de.schoolguy.libreverseback.CoordinateSystems;
import de.schoolguy.libreverseback.ReverseBackConverter;
import de.schoolguy.libreverseback.ReverseType;
import org.apache.commons.cli.*;

public class Main {

    public static void main(String[] args) {
        System.exit(program(args));
    }

    public static int program(String[] args) {
        Options options = new Options();

        options.addOption("1", true, "Erste Zeile der Reverse-Zahlen.");
        options.addOption("2", true, "Zweite Zeile der Reverse-Zahlen.");
        options.addOption("3", true, "Dritte Zeile der Reverse-Zahlen.");
        options.addOption("W", false, "Waldmeister Reverse Formal verwenden");
        options.addOption("M", false, "Maxipimpf Reverse Formel verwenden");

        String header = "Dieses Kommandozeilenprogramm berechnet Koordinaten für Waldmeister und Maximpimpf Reverse Wherigos.";
        String footer = "Fehler bitte nach https://gitlab.com/School_Guy/reverse-back-cli melden.";

        HelpFormatter formatter = new HelpFormatter();

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
            if (cmd.hasOption("1") && cmd.hasOption("2") && cmd.hasOption("3")
                    && (cmd.hasOption("W") || cmd.hasOption("M"))) {
                ReverseType type;
                if (cmd.hasOption("M")) {
                    type = ReverseType.MAXIPIMPF;
                } else {
                    type = ReverseType.WALDMEISTER;
                }
                ReverseBackConverter converter = new ReverseBackConverter(cmd.getOptionValue("1"), cmd.getOptionValue("2"), cmd.getOptionValue("3"), type);
                converter.calculate();
                String coordinates = converter.getFormattedCoords(CoordinateSystems.DECIMALDEGREEMINUTES);

                System.out.println("Ergebnis: " + coordinates);
            } else {
                System.err.println("Falsche Argumente! Hilfe wird ausgegeben.");
                formatter.printHelp("revesebackcli", header, options, footer, true);
            }
        } catch (ParseException e) {
            System.err.println("Kommandozeilenargumente konnten nicht geparst werden! Untenstehend ist eine Fehlermeldung für den Entwickler");
            System.err.println("");
            e.printStackTrace();
            return -1;
        }
        return 0;
    }
}
