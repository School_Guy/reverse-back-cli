import de.schoolguy.reversebackcli.Main;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

// CLI Tests from: https://stackoverflow.com/q/22545116/4730773
public class MainTests {

    // From https://stackoverflow.com/a/1119559/4730773
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;
    private final PrintStream originalErr = System.err;

    @BeforeEach
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @AfterEach
    public void restoreStreams() {
        System.setOut(originalOut);
        System.setErr(originalErr);
    }

    @Test
    void testEmtpy() {
        // Arrange
        String[] args = {};

        // Act
        Main.program(args);

        // Assert
        String[] testResult = outContent.toString().split("\\r?\\n");
        String[] expected = {"usage: revesebackcli [-1 <arg>] [-2 <arg>] [-3 <arg>] [-M] [-W]",
                "Dieses Kommandozeilenprogramm berechnet Koordinaten für Waldmeister und",
                "Maximpimpf Reverse Wherigos.",
                " -1 <arg>   Erste Zeile der Reverse-Zahlen.",
                " -2 <arg>   Zweite Zeile der Reverse-Zahlen.",
                " -3 <arg>   Dritte Zeile der Reverse-Zahlen.",
                " -M         Maxipimpf Reverse Formel verwenden",
                " -W         Waldmeister Reverse Formal verwenden",
                "Fehler bitte nach https://gitlab.com/School_Guy/reverse-back-cli melden."};
        Assertions.assertArrayEquals(expected, testResult);
    }

    @Test
    void testWaldmeister() {
        // Arrange
        String[] args = {"-W", "-1", "704193", "-2", "257681", "-3", "135670"};

        // Act
        Main.program(args);

        // Assert
        String[] testResult = outContent.toString().split("\\r?\\n");
        String expected = "Ergebnis: N 48 34.064 E 010 37.759";
        Assertions.assertEquals(expected, testResult[0]);
    }

    @Test
    void testMaxipimpf() {
        // Arrange
        String[] args = {"-M", "-1", "9717471", "-2", "9716611", "-3", "9416776"};

        // Act
        Main.program(args);

        // Assert
        String[] testResult = outContent.toString().split("\\r?\\n");
        String expected = "Ergebnis: N -5 45.163 W 282 01.673";
        Assertions.assertEquals(expected, testResult[0]);
    }

    @Test
    void testNonesense() {
        // Arrange
        String[] args = {"-W",};

        // Act
        Main.program(args);

        // Assert
        String[] testResult = errContent.toString().split("\\r?\\n");
        String expected = "Falsche Argumente! Hilfe wird ausgegeben.";
        Assertions.assertEquals(expected, testResult[0]);
    }
}
